﻿using IOTLabWebApi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IOTLabWebApi.Core.Models;

namespace IOTLabWebApi.Persistence
{
    public class LocalHubRepository : ILocalHubRepository
    {
        private readonly IotLabDbContext context;

        public LocalHubRepository(IotLabDbContext context)
        {
            this.context = context;
        }

        public bool AddUserLocalHub(LocalHub hub)
        {
            try
            {
                context.LocalHub.AddAsync(hub);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public LocalHub GetUserLocalHub(string useremail)
        {
            var localhub = context.LocalHub.FirstOrDefault(u => u.UserEmail == useremail);
            return localhub;
        }

        public async Task<LocalHub> GetUserLocalHubById(int id)
        {
            return await context.LocalHub.FindAsync(id);
        }
    }
}

using IOTLabWebApi.Core.Models;
using System.Threading.Tasks;

namespace IOTLabWebApi.Core
{
    public interface ILocalHubRepository
    {
        LocalHub GetUserLocalHub(string useremail);
        Task<LocalHub> GetUserLocalHubById(int id);
        bool AddUserLocalHub(LocalHub hub);
                                            
    }
}
namespace IOTLabWebApi.Core.Models
{
    public class LocalHub
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string HubIotAzureId { get; set; }
        public string SharedAccessKey { get; set; }
        public string UserEmail { get; set; }
    }
}
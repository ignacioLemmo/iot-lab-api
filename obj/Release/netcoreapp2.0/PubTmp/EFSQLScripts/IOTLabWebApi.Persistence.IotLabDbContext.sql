﻿IF OBJECT_ID(N'__EFMigrationsHistory') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170819224736_InitialModel')
BEGIN
    CREATE TABLE [Devices] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(255) NOT NULL,
        CONSTRAINT [PK_Devices] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170819224736_InitialModel')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20170819224736_InitialModel', N'2.0.0-rtm-26452');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170820234633_AddComDeviceId')
BEGIN
    ALTER TABLE [Devices] ADD [DeviceComId] nvarchar(255) NOT NULL DEFAULT N'';
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170820234633_AddComDeviceId')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20170820234633_AddComDeviceId', N'2.0.0-rtm-26452');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170821014817_AddModel')
BEGIN
    ALTER TABLE [Devices] ADD [ModelId] int NOT NULL DEFAULT 0;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170821014817_AddModel')
BEGIN
    CREATE TABLE [Models] (
        [Id] int NOT NULL IDENTITY,
        [AnalogInputPins] nvarchar(255) NOT NULL,
        [DigitalIOPins] nvarchar(255) NOT NULL,
        [ImgUrl] nvarchar(255) NOT NULL,
        [Microcontroller] nvarchar(255) NOT NULL,
        [MoreInfo] nvarchar(255) NOT NULL,
        [Name] nvarchar(255) NOT NULL,
        [PWMDigitalIOPins] nvarchar(255) NOT NULL,
        CONSTRAINT [PK_Models] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170821014817_AddModel')
BEGIN
    CREATE INDEX [IX_Devices_ModelId] ON [Devices] ([ModelId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170821014817_AddModel')
BEGIN
    ALTER TABLE [Devices] ADD CONSTRAINT [FK_Devices_Models_ModelId] FOREIGN KEY ([ModelId]) REFERENCES [Models] ([Id]) ON DELETE CASCADE;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170821014817_AddModel')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20170821014817_AddModel', N'2.0.0-rtm-26452');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170821014957_SedDbWhitModels')
BEGIN
    INSERT INTO Models (Name,ImgUrl,MoreInfo,Microcontroller,DigitalIOPins,AnalogInputPins,PWMDigitalIOPins) VALUES ('Arduino Uno','https://store-cdn.arduino.cc/usa/catalog/product/cache/1/image/1800x/ea1ef423b933d797cfca49bc5855eef6/A/0/A000066_front_2.jpg','https://store.arduino.cc/usa/arduino-uno-rev3','ATmega328P','14','6','6')
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170821014957_SedDbWhitModels')
BEGIN
    INSERT INTO Models (Name,ImgUrl,MoreInfo,Microcontroller,DigitalIOPins,AnalogInputPins,PWMDigitalIOPins) VALUES ('Arduino Nano','https://store-cdn.arduino.cc/usa/catalog/product/cache/1/image/1800x/ea1ef423b933d797cfca49bc5855eef6/A/0/A000005_front_2.jpg','https://store.arduino.cc/usa/arduino-nano','ATmega328','22','8','6')
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170821014957_SedDbWhitModels')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20170821014957_SedDbWhitModels', N'2.0.0-rtm-26452');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170824174732_AddUserAppProfile')
BEGIN
    CREATE TABLE [UserAppProfiles] (
        [Id] int NOT NULL IDENTITY,
        [Email] nvarchar(max) NULL,
        [Name] nvarchar(max) NULL,
        [PhoneNumber] nvarchar(max) NULL,
        CONSTRAINT [PK_UserAppProfiles] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170824174732_AddUserAppProfile')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20170824174732_AddUserAppProfile', N'2.0.0-rtm-26452');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170824221805_AddUpdateTimeToProfile')
BEGIN
    ALTER TABLE [UserAppProfiles] ADD [LastUpdate] datetime2 NOT NULL DEFAULT '0001-01-01T00:00:00.000';
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170824221805_AddUpdateTimeToProfile')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20170824221805_AddUpdateTimeToProfile', N'2.0.0-rtm-26452');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170825204425_UpdateProfile')
BEGIN
    DECLARE @var0 sysname;
    SELECT @var0 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'UserAppProfiles') AND [c].[name] = N'LastUpdate');
    IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [UserAppProfiles] DROP CONSTRAINT [' + @var0 + '];');
    ALTER TABLE [UserAppProfiles] DROP COLUMN [LastUpdate];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170825204425_UpdateProfile')
BEGIN
    DECLARE @var1 sysname;
    SELECT @var1 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'UserAppProfiles') AND [c].[name] = N'Name');
    IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [UserAppProfiles] DROP CONSTRAINT [' + @var1 + '];');
    ALTER TABLE [UserAppProfiles] DROP COLUMN [Name];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170825204425_UpdateProfile')
BEGIN
    DECLARE @var2 sysname;
    SELECT @var2 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'UserAppProfiles') AND [c].[name] = N'PhoneNumber');
    IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [UserAppProfiles] DROP CONSTRAINT [' + @var2 + '];');
    ALTER TABLE [UserAppProfiles] DROP COLUMN [PhoneNumber];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170825204425_UpdateProfile')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20170825204425_UpdateProfile', N'2.0.0-rtm-26452');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170830201136_AddLocalHub')
BEGIN
    CREATE TABLE [LocalHub] (
        [Id] int NOT NULL IDENTITY,
        [HubIotAzureId] nvarchar(max) NULL,
        [Name] nvarchar(max) NULL,
        [SharedAccessKey] nvarchar(max) NULL,
        [UserEmail] nvarchar(max) NULL,
        CONSTRAINT [PK_LocalHub] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20170830201136_AddLocalHub')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20170830201136_AddLocalHub', N'2.0.0-rtm-26452');
END;

GO


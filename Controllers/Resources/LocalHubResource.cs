﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IOTLabWebApi.Controllers.Resources
{
    public class LocalHubResource
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public string HubIotAzureId { get; set; }
        public string SharedAccessKey { get; set; }
        [Required]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$", ErrorMessage = "Debe ser un email valido")]
        public string UserEmail { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using IOTLabWebApi.Core;
using AutoMapper;
using IOTLabWebApi.Controllers.Resources;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using IOTLabWebApi.Core.Models;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace IOTLabWebApi.Controllers
{
    [Route("api/localhubcontroller")]
    [EnableCors("CorsPolicy")]
    public class LocalHubController : Controller
    {
        private readonly ILocalHubRepository repo;
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        private readonly HttpClient httpClient;

        public LocalHubController(ILocalHubRepository repo, IMapper mapper, IUnitOfWork unitOfWork, HttpClient httpClient)
        {
            this.unitOfWork = unitOfWork;
            this.httpClient = httpClient;
            this.repo = repo;
            this.mapper = mapper;
        }

        /// <summary>
        /// Get a Local Hub by email
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns>New Created userprofile</returns>
        /// <response code="200">Returns true and  userhub</response>
        /// <response code="200">Returns fale the result is null</response>
        /// <response code="400">Returns 400 error If the request did not pass validation</response>
        [ProducesResponseType(typeof(UserAppProfileResource), 200)]
        [HttpGet("getbyemail/{email}")]
        public IActionResult GetLocalHubEmail(string email)
        {

            if (email == null)
                return BadRequest("Se necesita email");

            var localhub = repo.GetUserLocalHub(email);

            var responseHub = new JObject();

            if (localhub == null) { 

                responseHub.Add("userHaveHub", false);

                return Ok(responseHub);
            }
            else{

                var result = mapper.Map<LocalHub, LocalHubResource>(localhub);

                responseHub.Add("userHaveHub", true);
                 
                responseHub.Add("hub", JObject.Parse(JsonConvert.SerializeObject(result)));

                return Ok(responseHub);
            }
        }

        /// <summary>
        /// Creates a User Local Hub
        /// </summary>
        /// <remarks>
        /// Todos los parametros son requeridos.
        ///  
        ///     POST /localhubcontroller
        ///     {
        ///        "name": "pepe honguito",
        ///        "useremail": "pepe.honguito@gmail.com",
        ///     }
        /// 
        /// </remarks>
        /// <param name="userProfile"></param>
        /// <returns>New Created userprofile</returns>
        /// <response code="200">Returns the newly created localhub</response>
        /// <response code="400">Returns 400 error If the request did not pass validation or error in generate hub id</response>
        [HttpPost]
        [ProducesResponseType(typeof(UserAppProfileResource), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(405)]
        public async Task<IActionResult> CreateLocalHub([FromBody] LocalHubResource localHubResource) {


            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var hubid = Guid.NewGuid().ToString().Substring(0, 12); ;
                
                HttpResponseMessage response = await httpClient
                    .GetAsync("https://messages-manager.azurewebsites.net/api/AddDeviceHub?code=WLHtBWu03IsLzcs8IZmMs5YSBM3sNhJJ11NH8cKRR75/qNdMH3fiWg==&deviceid="+ hubid);
                response.EnsureSuccessStatusCode();
                    
                string responseBody = await response.Content.ReadAsStringAsync();

                JObject jObject = JObject.Parse(responseBody);

                var localHub = mapper.Map<LocalHubResource, LocalHub>(localHubResource);
                localHub.HubIotAzureId= (string)jObject["deviceId"];
                localHub.SharedAccessKey= (string)jObject["authentication"]["symmetricKey"]["primaryKey"];

                if (repo.AddUserLocalHub(localHub))
                {

                    await unitOfWork.CompleteAsync();

                    localHub = await repo.GetUserLocalHubById(localHub.Id);

                    var result = mapper.Map<LocalHub, LocalHubResource>(localHub);

                    return Ok(result);
                }

                else {
                    return BadRequest("No se pudo crear hub");
                }

            }   
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
